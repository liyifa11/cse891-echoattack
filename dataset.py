import scipy.io as sio
import numpy as np
import os

def preprocess_mat(activate_mat_path, Vnoise_avg, w=10):
    """
    function: preprocess the RSS data, refer to Sec. 4.3
    input: activate_mat_path: the path of activated RSS matrix, 
           Vnoise_avg: the average of static noise RSS matrix (the voice assitant is not activated) in each channel,
           w: sliding window size
    output: final features (7 dimensions: maximum, minimum, and average peak values of 300ms before activation and after activation and the average RSS difference of 300ms before and after activation)
    """
    activate_mat = sio.loadmat(activate_mat_path)
    V_activate = activate_mat['data']
    
    #####Remove the RSS value that is lower than the average RSS value######
    Vnoise_avg = Vnoise_avg.reshape(-1, 1)
    V_activate[V_activate<Vnoise_avg] = -110
    
    #####Shrink the matrix to a RSS vector######
    E_activate = np.max(V_activate, axis=0)
    
    #####Sliding window to get the smoothed RSS feature F#####
    kernel = np.ones(w)/w
    F = np.convolve(E_activate, kernel, mode='valid')
    
    #####Extract features#####
    before_F, after_F = F[100:400], F[400:700]
    before_F_max, before_F_min, before_F_avg = before_F.max(), before_F.min(), before_F.mean()
    after_F_max, after_F_min, after_F_avg = after_F.max(), after_F.min(), after_F.mean()
    diff_F = (after_F - before_F).mean()
    final_features = np.ones(7)
    final_features[0], final_features[1], final_features[2], final_features[3], final_features[4], final_features[5], final_features[6] = before_F_max, before_F_min, before_F_avg, after_F_max, after_F_min, after_F_avg, diff_F
    return final_features


def cal_Vnoise_avg(noise_mats_path):
    if os.path.exists(noise_mats_path):
        noise_mats_paths = os.listdir(noise_mats_path)
        for i, noise_mat_path in enumerate(noise_mats_paths[:10]):
            noise_mat_path = f'{noise_mats_path}/{noise_mat_path}'
            if i==0:
                noise_mat = sio.loadmat(noise_mat_path)['data']
            else:
                noise_mat += sio.loadmat(noise_mat_path)['data']
        noise_mat=noise_mat/(i+1)
        Vnoise_avg = np.mean(noise_mat, axis=1)
        return Vnoise_avg
    

if __name__ == '__main__':
    activate_mat_path = './dataset/bixby/busy_1.mat'
    noise_mats_path = './dataset/idle/'
    Vnoise_avg = cal_Vnoise_avg(noise_mats_path)
    preprocess_mat(activate_mat_path=activate_mat_path, Vnoise_avg=Vnoise_avg)

