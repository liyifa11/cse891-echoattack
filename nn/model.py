from typing import Any

import lightning.pytorch as pl
import torch
from torch import Tensor, nn
from torch.optim.lr_scheduler import ExponentialLR, LambdaLR, SequentialLR
from torchmetrics import Accuracy
from torchmetrics.classification import MulticlassF1Score


class EchoBin(pl.LightningModule):
    def __init__(self, mode: str = 'hand-design'):
        super().__init__()

        # in channel
        in_c = 7 if mode == 'hand-design' else 1600
        self.mode = mode

        # encoder
        self.fc1 = nn.Sequential(
            nn.Linear(16 * in_c, 128),
            nn.BatchNorm1d(128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.BatchNorm1d(64),
            nn.ReLU(),
        )

        # data-driven weights
        self.time_att = nn.Sequential(
            nn.Linear(64, 64),
            nn.BatchNorm1d(64),
            nn.ReLU(),
        )

        # classifier
        self.fc2 = nn.Sequential(
            nn.Linear(64, 32),
            nn.BatchNorm1d(32),
            nn.ReLU(),
            nn.Linear(32, 1),
            nn.Sigmoid(),
        )

        # dropout
        self.drop = nn.Dropout(p=0.2)

        # matrix
        self.accuracy = Accuracy('binary')
        self.f1 = MulticlassF1Score(num_classes=2, average=None)

    def forward(self, x):
        # flat: [b, 16, 1600] -> [b, 16 * 1600]
        x = x.view(x.shape[0], -1)

        # encoder: [b, 16 * 1600] -> [b, 64]
        x = self.fc1(x)

        # data-driven weights: [b, 64] -> [b, 64] (weighted)
        w = self.time_att(x)
        w = nn.functional.softmax(w, dim=1)
        x = x * w

        # classifier: [b, 64] -> [b, 1]
        x = self.fc2(x)

        return x

    @staticmethod
    def smooth_matrix(matrix: Tensor) -> Tensor:
        # kernel
        kernel_size = 10
        kernel = torch.ones(1, 1, kernel_size, device=matrix.device) / kernel_size
        # conv
        matrix = matrix.view(-1, 1, 1600)
        matrix = torch.conv1d(matrix, kernel, padding='same')
        matrix = matrix.view(-1, 16, 1600)
        return matrix

    @staticmethod
    def normalize(matrix: Tensor) -> Tensor:
        # normalize
        matrix = matrix - matrix.min(dim=2, keepdim=True)[0]
        matrix = matrix / matrix.max(dim=2, keepdim=True)[0]
        return matrix

    @staticmethod
    def extract(matrix: Tensor) -> Tensor:
        # before act
        matrix_b = matrix[:, :, 100:400]
        matrix_a = matrix[:, :, 400:700]
        matrix_b_d = {'max': matrix_b.max(dim=2)[0], 'min': matrix_b.min(dim=2)[0], 'avg': matrix_b.mean(dim=2)}
        matrix_a_d = {'max': matrix_a.max(dim=2)[0], 'min': matrix_a.min(dim=2)[0], 'avg': matrix_a.mean(dim=2)}
        diff = matrix_a_d['avg'] - matrix_b_d['avg']
        return torch.stack(
            [
                matrix_b_d['max'], matrix_b_d['min'], matrix_b_d['avg'],
                matrix_a_d['max'], matrix_a_d['min'], matrix_a_d['avg'],
                diff,
            ], dim=2
        )

    def transform(self, matrix: Tensor) -> Tensor:
        # preprocess
        matrix = self.smooth_matrix(matrix)
        matrix = self.normalize(matrix)
        matrix = self.extract(matrix) if self.mode == 'hand-design' else matrix
        return matrix

    def training_step(self, batch: Any, batch_idx: int) -> Tensor:
        # unpack the batch
        matrix, label = batch

        # preprocess
        matrix = self.transform(matrix)

        # forward
        out = self.forward(matrix)
        out = self.forward(matrix)

        # loss
        loss = nn.functional.binary_cross_entropy(out, label, reduction='mean')
        self.log('loss', loss, on_epoch=True)
        return loss

    def validation_step(self, batch: Any, batch_idx: int):
        # unpack the batch
        # matrix: [b, 16, 6000]
        # label: [b, 1]
        matrix, label = batch

        # preprocess
        matrix = self.transform(matrix)

        # forward
        out = self.forward(matrix)
        preds = torch.round(out).int()

        # accuracy
        acc = self.accuracy(preds, label.int())

        # f1-score
        f1 = self.f1(preds, label.int())

        # log
        self.log_dict(
            {
                'val/acc': acc,
                'val/f1_t': f1[1],
                'val/f1_f': f1[0],
            },
            on_epoch=True
        )

    def predict_step(self, batch: Any, batch_idx: int) -> Tensor:
        # unpack the batch
        matrix = batch

        # preprocess
        matrix = self.transform(matrix)

        # forward
        out = self.forward(matrix)
        return out

    def configure_optimizers(self):
        optimizer = torch.optim.AdamW(self.parameters(), lr=1e-3)

        def warmup_fn(epoch):
            if epoch < 4:
                return float(epoch + 1) / 4
            return 1

        # warmup scheduler
        warmup_s = LambdaLR(optimizer, lr_lambda=warmup_fn)

        # cosine annealing scheduler
        expon_s = ExponentialLR(optimizer, 0.9)

        # merge two schedulers
        seq_s = SequentialLR(optimizer, [warmup_s, expon_s], [4])

        return {'optimizer': optimizer, 'lr_scheduler': seq_s}


if __name__ == '__main__':
    model = EchoBin()
    fake_i = torch.rand(2, 16, 6000)
    fake_o = model.forward(fake_i)
    print(fake_o)
