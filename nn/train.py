import logging
from argparse import ArgumentParser

import lightning.pytorch as pl
import torch
from lightning.pytorch.callbacks import ModelCheckpoint, ModelSummary, RichProgressBar
from lightning.pytorch.loggers import WandbLogger

from cluster import EchoCluster
from model import EchoBin


def train():
    # logging
    logging.basicConfig(level='INFO')

    # parse args
    args = ArgumentParser()
    args.add_argument('--cls', type=str, default='siri')
    args.add_argument('--proot', type=str, default='/egr/research-actionlab/huang247/iot/echoattack/dataset')
    args.add_argument('--mode', type=str, default='hand-design')
    args = args.parse_args()

    # fix random seed
    pl.seed_everything(42)

    # matmul precision
    torch.set_float32_matmul_precision('high')

    # keep no-nan
    torch.autograd.set_detect_anomaly(True)

    # model
    model = EchoBin(args.mode)

    # callbacks
    callbacks = [
        ModelCheckpoint(save_last=True, auto_insert_metric_name=True, save_top_k=5, monitor='val/acc', mode='max'),
        RichProgressBar(),
        ModelSummary(),
    ]

    # wandb
    wandb_l = WandbLogger(project='echo-attack', log_model='all')

    # init dataset
    cluster = EchoCluster(args.proot, args.cls)

    # train
    trainer = pl.Trainer(
        logger=wandb_l, callbacks=callbacks,
        max_epochs=40,
        accelerator='gpu', devices=[4, 5], strategy='ddp', benchmark=True,
    )
    trainer.fit(model, datamodule=cluster)


if __name__ == '__main__':
    train()
