import logging
from pathlib import Path

import lightning.pytorch as pl
import scipy
from torch import Tensor
from torch.utils.data import DataLoader, Dataset, random_split


class EchoDataset(Dataset):
    def __init__(self, p_root: str | Path, cls: str):
        super().__init__()

        # set attributes
        self.p_root = Path(p_root)

        # sample list
        self.l_sample = sorted([x for x in self.p_root.glob('**/*.mat') if x.parent.name in ['idle', cls]])
        logging.info(f'load {len(self.l_sample)} samples')

        # count positive and negative
        n_pos = len([x for x in self.l_sample if cls == x.parent.name])
        n_neg = len([x for x in self.l_sample if 'idle' == x.parent.name])
        logging.info(f'positive: {n_pos}, negative: {n_neg}')

    def __len__(self) -> int:
        return len(self.l_sample)

    def __getitem__(self, index: int) -> tuple[Tensor, Tensor]:
        # load sample
        p_sample = self.l_sample[index]
        sample = scipy.io.loadmat(str(p_sample))

        # label
        label = 'idle' != p_sample.parent.name
        label = Tensor([label]).float()

        # matrix
        data = sample['data']
        data = Tensor(data).float()

        return data, label


class EchoCluster(pl.LightningDataModule):
    def __init__(self, p_root: str | Path, cls: str):
        super().__init__()

        # dataset
        dataset = EchoDataset(p_root, cls)

        # loader
        div = 0.85
        self.t, self.v = random_split(dataset, [int(len(dataset) * div), len(dataset) - int(len(dataset) * div)])

    def train_dataloader(self):
        return DataLoader(self.t, batch_size=32, shuffle=True, num_workers=32)

    def val_dataloader(self):
        return DataLoader(self.v, batch_size=32, shuffle=False, num_workers=32)


if __name__ == '__main__':
    # dataset
    dataset = EchoDataset('/egr/research-actionlab/huang247/iot/echoattack/dataset', 'siri')

    # sample
    data, label = dataset[0]
    print(data.shape, label.shape)
